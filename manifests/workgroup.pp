# This module installs the LDAP, PAM, and NSS support required to
# use an LDAP directory as the source for Posix account and group
# information.  If needed this module installs a local LDAP proxy
# server that uses a Kerberos bind to the central service and
# allows anonymous local access to the directory information.
#
# There are two steps require to implement Workgroup control:
#
#   1. Obtain access the directory data for the workgroup.  This
#      access is granted to any administrator for a workgroup and does
#      not require Data Owner approval.  See the ldap ikiwiki pages
#      for the details of creating the access group.  Once the access
#      group exists the ldap auth remctl command can be used to grant
#      access.  For example,
#
#      % remctl ldap-master ldap auth host/foo.stanford.edu accessgroup rra
#
#
#   2. Configure any hosts that need the access control.  That
#      is what this module does.
#
# In addition to controlling access to a system using data from the
# cn=Accounts branch of the directory, Posix group information can
# be pulled from the cn=groups branch of the directory.  Once the
# directory ACL structures are created a gidNumber needs to be
# assigned to the Workgroups that will also be used as Posix groups.
# The required command is:
#
#      % remctl ldap-master ldap group add --type=posix stem:group
#
# Examples:
#
#   To restrict access to a single Workgoup and to pull a single group
#   from the LDAP directory define account and group filters.
#
#      ldap::workgroup { 'anesthesia':
#        ensure              => 'present',
#        ldap_account_filter => '(&(objectClass=posixAccount)(suPrivilegeGroup=stanford:staff))',
#        ldap_group_filter   => '(&(objectclass=suPosixGroup)(cn=stanford:staff)',
#        ldap_account_map    => {
#            'cn'            => 'suGroupCN',
#            'homeDirectory' => '"/home/$uid"',
#            'loginShell     => '"/bin/bash"',
#        },
#        keytab              => '/etc/nslcd.keytab',
#        principal           => 'service/nslcd'
#      }
#
#   All the settings can be configured in hiera like this:
#
#      ldap::workgroup::ldap_host: 'ldap.stanford.edu'
#      ldap::workgroup::base: 'dc=stanford,dc=edu'
#      ldap::workgroup::ldap_account_filter: '(&(objectclass=posixAccount)(suPrivilegeGroup=stanford::staff))'
#      ldap::workgroup::ldap_group_filter: '(&(objectClass=suPosixGroup)(cn=stanford:staff)'
#      ldap::workgroup::ldap_account_map: 
#        homeDirectory: '"/home/$uid"'
#        loginShell: '"/bin/bash"'
#      ldap::workgroup::ldap_group_map: 
#        cn: suGroupCN
#      ldap::workgroup::keytab: '/etc/nslcd.keytab'
#      ldap::workgroup::principal: 'service/nslcd' 
#
#   To allow anyone in the workgroup stem access an LDAP filter does
#   not need to be specified.
#
#      ldap::workgroup { 'anesthesia': ensure => 'present' }
#
# Testing:
#
#   To make sure that the correct account information is being
#   return the getent command can be used.  For example:
#
#     % getent passwd whm
#     % getent group somegroup
#
#   should return all of the posixAccount entries for the choosen
#   Workgroup stem.

class ldap::workgroup (
  $ensure              = 'present',
  $ldap_host           = 'ldap.stanford.edu',
  $ldap_search_base    = 'dc=stanford,dc=edu',
  $ldap_account_base   = 'cn=accounts,dc=stanford,dc=edu',
  $ldap_group_base     = 'cn=groups,dc=stanford,dc=edu',
  $ldap_account_filter = '(objectclass=posixAccount)',
  $ldap_group_filter   = '(objectclass=suPosixGroup)',
  $ldap_account_map    = {},
  $ldap_group_map      = {},
  $principal           = 'HOST',
  $keytab              = '/etc/krb5.keytab',
  $tmpl_nslcd          = 'ldap/etc/nslcd.conf.erb',
  $tmpl_nsswitch       = 'ldap/etc/nsswitch.conf.erb',
) {
  # no support for rhel6
  if ($::osfamily == 'RedHat') and (versioncmp($facts['os']['release']['major'], '7') < 0) {
    fail('RedHat family OSes are supported only starting version 7.')
  }

  case $ensure {
    'present': {
      $service_ensure = 'running'
      $service_enable = true
      $directory_ensure = 'directory'
      $package_ensure = 'present'

      file { '/etc/nsswitch.conf':
        content => template($tmpl_nsswitch),
      }
    }
    'absent': {
      $service_ensure = 'stopped'
      $service_enable = false
      $directory_ensure = 'absent'

      # remove configuraion files on debian
      $package_ensure = $::osfamily ? {
        'Debian' => 'purged',
        default => 'absent',
      }

      # keep nsswitch file after the mop up, but remove
      # ldap as a data source from it.
      exec { 'remove ldap from nsswitch.conf':
        command  => "perl -pe 's/ldap//' -i /etc/nsswitch.conf",
        provider => shell,
        onlyif   => 'grep ldap /etc/nsswitch.conf',
      }

      # remove ldap kerberos ticket
      file { '/var/run/nslcd/ldap.tgt':
        ensure => absent,
      }
    }
  }

  # packages needed for each os family
  $reqpkgs = $::osfamily ? {
    'RedHat' => [ 'nss-pam-ldapd', 'cyrus-sasl-ldap',
                  'cyrus-sasl', 'cyrus-sasl-gssapi' ],
    'Debian' => [ 'libpam-ldapd', 'libnss-ldapd', 'nslcd', 'nscd',
                  'libsasl2-modules-gssapi-mit', 'ldap-utils',
                  'nslcd-utils' ],
    default  => fail("OS family ${::osfamily} is not supported."),
  }

  # kerberos ticket for ldap authentication would be stored here
  $ticket_file = '/var/run/nslcd/ldap.tgt'

  package { $reqpkgs: ensure => $package_ensure; }

  # run nslcd as this user
  $this_uid = 'nslcd'

  # setup services
  if ($::osfamily == 'Debian')  {
    # run nslcd under this group
    $this_gid = 'nslcd'

    if ($ensure == 'present') {
      # Edit the nslcd init script to put the desired keytab
      # name and service principal. If ldap binding is absent,
      # just leave the file be, as the package manger would
      # remvoe it anyway.
      file_line { 'k5start_keytab':
        ensure => 'present',
        path   => '/etc/init.d/nslcd',
        match  => '^K5START_KEYTAB=',
        line   => "K5START_KEYTAB=${keytab}",
      }
      -> file_line { 'k5start_princ':
        ensure => 'present',
        path   => '/etc/init.d/nslcd',
        match  => '^K5START_PRINCIPAL=',
        line   => "K5START_PRINCIPAL=${principal}",
        notify => Service['nslcd'],
      }
    }
  }

  if $::osfamily == 'RedHat' {
    # run nslcd under this group
    $this_gid = 'ldap'

    # On redhat nslcd does not invoke k5start automatically.
    # We need to run it ourselves
    k5start { 'nslcd':
      ensure       => $ensure,
      description  => "Keep a ticket ${ticket_file} for nslcd",
      keytab       => $keytab,
      ticket_file  => $ticket_file,
      principal    => $principal,
      start_before => [ 'nslcd.service' ],
      owner        => $this_uid,
      group        => $this_gid,
      mode         => '0600',
      require      => File['/var/run/nslcd'],
    }
  }

  file {
    '/etc/ldap':
      ensure => $directory_ensure,
      backup => false,
      mode   => '0755';
    '/etc/ldap/ldap.conf':
      ensure  => $ensure,
      mode    => '0644',
      content => template('ldap/etc/ldap/ldap.conf.erb'),
      require => File['/etc/ldap'];
    '/var/run/nslcd':
      ensure  => $directory_ensure,
      backup  => false,
      mode    => '0755',
      owner   => $this_uid,
      group   => $this_gid,
      require => Package[$reqpkgs];
  }

  service { 'nslcd':
    ensure     => $service_ensure,
    enable     => $service_enable,
    hasstatus  => true,
    hasrestart => true,
    require    => Package[$reqpkgs],
  }

  file { '/etc/nslcd.conf':
    ensure  => $ensure,
    content => template($tmpl_nslcd),
    notify  => Service['nslcd'],
    require => Package[$reqpkgs],
  }
}

