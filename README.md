LDAP
----

This module installs the LDAP, PAM, and NSS support required to
use an LDAP directory as the source for Posix account and group
information.  If needed this module installs a local LDAP proxy
server that uses a Kerberos bind to the central service and
allows anonymous local access to the directory information.

There are two steps require to implement Workgroup control:

1. Obtain access the directory data for the workgroup.  This
access is granted to any administrator for a workgroup and does
not require Data Owner approval.  See the ldap ikiwiki pages
for the details of creating the access group.  Once the access
group exists the ldap auth remctl command can be used to grant
access.  For example,

```
   % remctl ldap-master ldap auth host/foo.stanford.edu accessgroup rra
```

2. Configure any hosts that need the access control.  That
is what this module does.

In addition to controlling access to a system using data from the
cn=Accounts branch of the directory, Posix group information can
be pulled from the cn=groups branch of the directory.  Once the
directory ACL structures are created a gidNumber needs to be
assigned to the Workgroups that will also be used as Posix groups.
The required command is:

```
     % remctl ldap-master ldap group add --type=posix stem:group
```

## Examples:

To restrict access to a single Workgoup and to pull a single group
from the LDAP directory define account and group filters.

```
ldap::workgroup { 'anesthesia':
  ensure              => 'present',
  ldap_account_filter => '(&(objectClass=posixAccount)(suPrivilegeGroup=stanford:staff))',
  ldap_group_filter   => '(&(objectclass=suPosixGroup)(cn=stanford:staff)',
  ldap_account_map    => {
      'cn'            => 'suGroupCN',
      'homeDirectory' => '"/home/$uid"',
      'loginShell     => '"/bin/bash"',
  },
  keytab              => '/etc/nslcd.keytab',
  principal           => 'service/nslcd'
}
```

All the settings can be configured in hiera like this:

```
ldap::workgroup::ldap_host: 'ldap.stanford.edu'
ldap::workgroup::base: 'dc=stanford,dc=edu'
ldap::workgroup::ldap_account_filter: '(&(objectclass=posixAccount)(suPrivilegeGroup=stanford::staff))'
ldap::workgroup::ldap_group_filter: '(&(objectClass=suPosixGroup)(cn=stanford:staff)'
ldap::workgroup::ldap_account_map: 
  homeDirectory: '"/home/$uid"'
  loginShell: '"/bin/bash"'
ldap::workgroup::ldap_group_map: 
  cn: suGroupCN
ldap::workgroup::keytab: '/etc/nslcd.keytab'
ldap::workgroup::principal: 'service/nslcd' 
```

To allow anyone in the workgroup stem access an LDAP filter does
not need to be specified.

```
ldap::workgroup { 'anesthesia': ensure => 'present' }
```

